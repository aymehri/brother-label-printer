var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Plugin, Cordova, IonicNativePlugin } from '@ionic-native/core';
/**
 * @name Brother Label Printer
 * @description
 * This plugin does something
 *
 * @usage
 * ```typescript
 * import { BrotherLabelPrinter } from '@ionic-native/brother-label-printer';
 *
 *
 * constructor(private brotherLabelPrinter: BrotherLabelPrinter) { }
 *
 * ...
 *
 *
 * this.brotherLabelPrinter.functionName('Hello', 123)
 *   .then((res: any) => console.log(res))
 *   .catch((error: any) => console.error(error));
 *
 * ```
 */
var BrotherLabelPrinter = (function (_super) {
    __extends(BrotherLabelPrinter, _super);
    function BrotherLabelPrinter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * This function find all available wifi printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    /**
       * This function find all available wifi printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    BrotherLabelPrinter.prototype.findNetworkPrinters = /**
       * This function find all available wifi printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    function () {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    /**
    * This function find all available bluetooth printers
    * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
    */
    /**
      * This function find all available bluetooth printers
      * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
      */
    BrotherLabelPrinter.prototype.findBluetoothPairedPrinters = /**
      * This function find all available bluetooth printers
      * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
      */
    function () {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    BrotherLabelPrinter.prototype.printViaWifiInfra = /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    function (data, numberOfCopies, ipAddress, macAddress) {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    BrotherLabelPrinter.prototype.printViaWifiInfraText = /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    function (data, numberOfCopies, ipAddress, macAddress, modelName, labelNameIndex) {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    BrotherLabelPrinter.prototype.printViaSDK = /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    function (data, numberOfCopies, macAddress) {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    BrotherLabelPrinter.prototype.printViaSDKText = /**
       * This function find all available bluetooth printers
       * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
       */
    function (data, numberOfCopies, macAddress, modelName, labelNameIndex) {
        return; // We add return; here to avoid any IDE / Compiler errors
    };
    BrotherLabelPrinter.decorators = [
        { type: Injectable },
    ];
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], BrotherLabelPrinter.prototype, "findNetworkPrinters", null);
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", Promise)
    ], BrotherLabelPrinter.prototype, "findBluetoothPairedPrinters", null);
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Number, String, String]),
        __metadata("design:returntype", Promise)
    ], BrotherLabelPrinter.prototype, "printViaWifiInfra", null);
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Number, String, String, String, String]),
        __metadata("design:returntype", Promise)
    ], BrotherLabelPrinter.prototype, "printViaWifiInfraText", null);
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Number, String]),
        __metadata("design:returntype", Promise)
    ], BrotherLabelPrinter.prototype, "printViaSDK", null);
    __decorate([
        Cordova(),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, Number, String, String, String]),
        __metadata("design:returntype", Promise)
    ], BrotherLabelPrinter.prototype, "printViaSDKText", null);
    /**
     * @name Brother Label Printer
     * @description
     * This plugin does something
     *
     * @usage
     * ```typescript
     * import { BrotherLabelPrinter } from '@ionic-native/brother-label-printer';
     *
     *
     * constructor(private brotherLabelPrinter: BrotherLabelPrinter) { }
     *
     * ...
     *
     *
     * this.brotherLabelPrinter.functionName('Hello', 123)
     *   .then((res: any) => console.log(res))
     *   .catch((error: any) => console.error(error));
     *
     * ```
     */
    BrotherLabelPrinter = __decorate([
        Plugin({
            pluginName: 'BrotherLabelPrinter',
            plugin: 'cordova-brother-label-printer',
            // npm package name, example: cordova-plugin-camera
            pluginRef: 'plugin.brotherPrinter',
            // the variable reference to call the plugin, example: navigator.geolocation
            repo: '',
            // the github repository URL for the plugin
            install: '',
            // OPTIONAL install command, in case the plugin requires variables
            installVariables: [],
            // OPTIONAL the plugin requires variables
            platforms: ['Android'] // Array of platforms supported, example: ['Android', 'iOS']
        })
    ], BrotherLabelPrinter);
    return BrotherLabelPrinter;
}(IonicNativePlugin));
export { BrotherLabelPrinter };
//# sourceMappingURL=index.js.map