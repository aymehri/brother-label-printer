import { IonicNativePlugin } from '@ionic-native/core';
/**
 * @name Brother Label Printer
 * @description
 * This plugin does something
 *
 * @usage
 * ```typescript
 * import { BrotherLabelPrinter } from '@ionic-native/brother-label-printer';
 *
 *
 * constructor(private brotherLabelPrinter: BrotherLabelPrinter) { }
 *
 * ...
 *
 *
 * this.brotherLabelPrinter.functionName('Hello', 123)
 *   .then((res: any) => console.log(res))
 *   .catch((error: any) => console.error(error));
 *
 * ```
 */
export declare class BrotherLabelPrinter extends IonicNativePlugin {
    /**
     * This function find all available wifi printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    findNetworkPrinters(): Promise<any>;
    /**
    * This function find all available bluetooth printers
    * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
    */
    findBluetoothPairedPrinters(): Promise<any>;
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    printViaWifiInfra(data: string, numberOfCopies: number, ipAddress: string, macAddress: string): Promise<any>;
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    printViaWifiInfraText(data: string, numberOfCopies: number, ipAddress: string, macAddress: string, modelName: string, labelNameIndex: string): Promise<any>;
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    printViaSDK(data: string, numberOfCopies: number, macAddress: string): Promise<any>;
    /**
     * This function find all available bluetooth printers
     * @return {Promise<IPrinterWifi>} Returns a promise that resolves when something happens
     */
    printViaSDKText(data: string, numberOfCopies: number, macAddress: string, modelName: string, labelNameIndex: string): Promise<any>;
}
export interface IPrinterWifi {
    ipAddress: string;
    macAddress: string;
    serNo: string;
    nodeName: string;
}
export interface IPrinterBluetooth {
    address: string;
    name: string;
    type: string;
}
